package LSP.exercise_refactored;

public class BiologicalDuck extends Duck{

	@Override
	public void quack() {
		System.out.println("Quack...");
		
	}

	@Override
	public void swim() {
		System.out.println("Swim...");
		
	}

}
