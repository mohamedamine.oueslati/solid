package LSP.exercise_refactored;

public abstract class Duck {
	public abstract void quack();
	public abstract void swim();
}
