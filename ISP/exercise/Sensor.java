package ISP.exercise_refactored;

import java.util.Random;

import ISP.exercise_refactored.PDoor;

public class Sensor
{
    public void register(PDoor door)
    {
        while (true) {
            if (isPersonClose()) {
                door.proximityCallback();
                break;
            }
        }
    }

    private boolean isPersonClose()
    {
        return new Random().nextBoolean();
    }
}
