package DIP.exercise_refactored;

import java.io.IOException;

public class EncodingModuleClient {

	public static void main(String[] args) throws IOException {
		EncodingFileModule encodingFileModule = new EncodingFileModule();
		EncodingDBModule encodingDBModule = new EncodingDBModule();
		encodingFileModule.Encoding();
		encodingDBModule.Encoding();

	}

}
