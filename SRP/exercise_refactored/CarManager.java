package SRP.exercise_refactored;

import java.util.Arrays;
import java.util.List;

import SRP.exercise_refactored.Car;

public class CarManager {
	private CarDBO carsDbo = new CarDBO();

	private List<Car> getCars() {
		return carsDbo.get_carsDb();
	}

	public String getCarsNames() {
		StringBuilder sb = new StringBuilder();
		for (Car car : getCars()) {
			sb.append(car.getBrand());
			sb.append(" ");
			sb.append(car.getModel());
			sb.append(", ");
		}
		return sb.substring(0, sb.length() - 2);
	}

	public Car getBestCar() {
		Car bestCar = null;
		for (Car car : getCars()) {
			if (bestCar == null || car.getModel().compareTo(bestCar.getModel()) > 0) {
				bestCar = car;
			}
		}
		return bestCar;
	}
}
