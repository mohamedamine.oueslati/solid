package OCP.exercise_refactored;

public class SpaceSlotDBO extends SlotDBO{
	

	public SpaceSlotDBO() {
		super();
		
	}

	@Override
	public Resource findFreeSlot() {
		// TODO Auto-generated method stub
		return new SpaceSlot();
	}

	@Override
	public void markSlotBusy(Resource resource) {
		resource.markSlotBusy();
	}

	@Override
	public void markSlotFree(Resource resource) {
		resource.markSlotFree();
		
	}

	@Override
	public int ResourceId(Resource resource) {
		// TODO Auto-generated method stub
		return 0;
	}
}
