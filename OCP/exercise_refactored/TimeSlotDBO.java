package OCP.exercise_refactored;

public class TimeSlotDBO extends SlotDBO{
	
	public TimeSlotDBO() {
		super();
		
	}


	@Override
	public Resource findFreeSlot() {
		// TODO Auto-generated method stub
		return new TimeSlot();
	}


	@Override
	public void markSlotBusy(Resource resource) {
		resource.markSlotBusy();
		
	}


	@Override
	public void markSlotFree(Resource resource) {
		resource.markSlotFree();
		
	}


	@Override
	public int ResourceId(Resource resource) {
		// TODO Auto-generated method stub
		return 0;
	}
}
