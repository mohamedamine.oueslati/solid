package OCP.exercise_refactored;

public abstract class Resource {
	
	public abstract void markSlotFree();
	public abstract void markSlotBusy();
}
