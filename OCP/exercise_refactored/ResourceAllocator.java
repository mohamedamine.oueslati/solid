package OCP.exercise_refactored;



public class ResourceAllocator {
	private static final int INVALID_RESOURCE_ID = -1;

    public int allocate(SlotDBO Slot)
    {
    	Resource resource = Slot.findFreeSlot();
        Slot.markSlotBusy(resource);
        
        return Slot.ResourceId(resource);
    }

    public void free(Resource resource)
    {
    	resource.markSlotFree();
    }
}
