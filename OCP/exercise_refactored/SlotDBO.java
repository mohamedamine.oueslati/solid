package OCP.exercise_refactored;

public abstract class SlotDBO {
	
	public abstract Resource findFreeSlot();
	public abstract int ResourceId(Resource resource);
	public abstract void markSlotBusy(Resource resource);
	public abstract void markSlotFree(Resource resource);
}
